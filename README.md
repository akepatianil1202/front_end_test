# Test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.

## Starting the server and running the application

Run `ng serve` command which will compile all files and also it will start the dev server.
Please Navigate to `http://localhost:4200/` to see the test application

#Tree Traversal

I have written below methods:

1.moveElementsIntoODArray -- This will move all elements into one dimentional array
2.get(id) -- which will return the node object which has unque id assigned
3.insert(parentId, element, nodeArray) -- This will insert element into the nodeArray

#DOM Styling

"SYS:food", "PAR:ANIL", "ACT:net" -- System defined -- every prefix has unque colour which is applying dynamically based on the prefix
food -- User defined -- when we don't have a prefix then it will have one color

#CSS Styling
Create a table which will be displaying the full table if the resolution is more than 96opx.
And if it is less than 960px then the mobile view will be displaying as given in the task.

## Running Unit Tests

Run `ng test` to run the tests and see the results


