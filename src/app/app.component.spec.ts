import { TestBed, ComponentFixture ,async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import treeJson from '../app/tree.json';
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [AppComponent ],
    }).compileComponents();
    // create component and test fixture
    fixture = TestBed.createComponent(AppComponent);
    // get test component from the fixture
    component = fixture.componentInstance;
    component.ngOnInit();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as single onedimensinal array`, () => {
    component.moveElementsIntoODArray(treeJson)
    expect(typeof treeJson).toEqual('object');
  });
  it(`should get an parent object based on id`, () => {
    let getParentObject=component.get("c64ac3d7-5e88-40ed-8d6f-e7f10d21c5bc")
    expect(getParentObject.id).toEqual('c64ac3d7-5e88-40ed-8d6f-e7f10d21c5bc');
  });
  it(`should insert a new node based on element`, () => {
      let element={
        "name": "RENAMING a",
        "parentFolderId": "1e5196f3-c3f9-34db-aaab-8efdd63dd5b0",
        "fixtureId": null,
        "mailboxId": null,
        "cChatChannelId": null,
        "archived": false,
        "systemRoomFolder": false,
        "adminUserConfigurationRoleId": null,
        "readConfigurationRoleId": null,
        "externalConfigurationRoleId": null,
        "favouritesForUsers": [],
        "childrenCount": 1,
        "id": "ecac3eed-6691-4e95-9e3e-30ac5b725fca"
    }
    let intialLength=treeJson.length;
    component.insert("c64ac3d7-5e88-40ed-8d6f-e7f10d21c5bc",element,treeJson)
    expect(intialLength).toBeLessThan(treeJson.length);
  });

  
});
