import { Component, OnInit } from '@angular/core';
import treeJson from '../app/tree.json';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'test';
  data: any;
  domLayout: any = [];
  oneDimensionalArray : any=[];

  constructor() {
    this.oneDimensionalArray=treeJson;
  }

  ngOnInit() {

    //DOM Styling
    this.data = { "message": ["SYS:food", "PAR:ANIL", "ACT:net","food"] };
    this.domLayoutFun();

   //Tree Traversal
    this.moveElementsIntoODArray(this.oneDimensionalArray)
    let id="c64ac3d7-5e88-40ed-8d6f-e7f10d21c5bc";
    this.get(id)
    let element={
      "name": "RENAMING a",
      "parentFolderId": "1e5196f3-c3f9-34db-aaab-8efdd63dd5b0",
      "fixtureId": null,
      "mailboxId": null,
      "cChatChannelId": null,
      "archived": false,
      "systemRoomFolder": false,
      "adminUserConfigurationRoleId": null,
      "readConfigurationRoleId": null,
      "externalConfigurationRoleId": null,
      "favouritesForUsers": [],
      "childrenCount": 1,
      "id": "ecac3eed-6691-4e95-9e3e-30ac5b725fca"
  }
    this.insert(id, element, this.oneDimensionalArray) 
  }

// Tree Travesel --> create a singal dimensional collection
  moveElementsIntoODArray(a) {
    for (var i in a) {
      if (a[i].children && a[i].children.length > 0) {
        var childs = a[i].children
        for (var j in childs) {
          childs[j]['parentId'] = a[i].id
          a.push(childs[j])
        }
        delete a[i].children;
      }
      a[i]['parentId'] = null
    }  
  }

// Tree Travesel --> Get a Element based on id
  get(id) {
    for(var i in this.oneDimensionalArray) {
      if(this.oneDimensionalArray[i].id === id) {
        if(this.oneDimensionalArray[i].parentId) {
          for(var j in this.oneDimensionalArray) {
            if(this.oneDimensionalArray[j].id === this.oneDimensionalArray[i].parentId) {
              return this.oneDimensionalArray[j]
            } else {
              return this.oneDimensionalArray[i];
            }
          }
        } else {
          return this.oneDimensionalArray[i]
        }
      }
    }
  }

// Tree Travesel --> Insert a Element
  insert(id, element, a) {
    element.parentId = id
    a.push(element)
  }


  // DOM STYLING method
  domLayoutFun() {
    this.data.message.map(str => {
      let strSplit = str.split(":");
      if(strSplit.length>1){
        this.domLayout.push({ key: strSplit[0], value: strSplit[1] })
      }else{
        this.domLayout.push({ key: "", value: strSplit[0] })
      }
    })
  }

// DOM STYLING --> DYNAMIC APPENDING A COLOR
  getColor(country) {
    let color=null;
    switch (country) {
      case 'SYS':
        color='green'
        break;
      case 'PAR':
          color='blue'
          break;
      case 'ACT':
          color='red'
          break;
      case '':
          color='yellow'
          break;
      default:
          color='grey'
          break;
    }
    return color;
  }

}
